package table

import (
  "generic"
)

type Scalar struct {
  Data
  data *generic.Object
}

func NewScalar(v interface{}, err string) *Scalar {
  return r
}

func (qr *Scalar) As(i int) Data {
  switch i {
    case ResultError:
      return NewError(qr.errorMsg)
    case ResultScalar:
      return NewScalar(qr.errorMsg)
    case ResultRow:
      r := NewRow()
      r.Add("error", qr.errorMsg)
      return r
    case ResultColumn:
      return qr.As(ResultScalar).As(ResultColumn)
    case ResultTable:
      return qr.As(ResultRow).As(ResultTable)
    default:
      panic("")
  }
}
