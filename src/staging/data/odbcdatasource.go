package data

import (
  "odbc"
)

type ODBCDataSource struct {
  DataSource
  dsn string
  odbcConn *odbc.Connection
  isTr bool
}

func NewODBCDataSource(dsn string) *ODBCDataSource {
  ds := new(ODBCDataSource)
  ds.dsn = dsn
  ds.isTr = false
  ds.odbcConn = nil
  ds.Connect()
  return ds
}

func (ds *ODBCDataSource) Connect() {
  if ds.odbcConn != nil {
    ds.Close(true)
  }
  var err *odbc.ODBCError = nil
  ds.odbcConn, err = odbc.Connect(ds.dsn)
  if err != nil {
    panic("can't connect odbc " + err.String())
  }
}

func (ds *ODBCDataSource) Begin() {
  if ds.odbcConn == nil {
    ds.Connect()
  }
  ds.Query("BEGIN");
  ds.isTr = true
}

func (ds *ODBCDataSource) Rollback() {
  if ds.odbcConn != nil && ds.isTr {
    ds.Query("ROLLBACK");
  }
  ds.isTr = false
}

func (ds *ODBCDataSource) Commit() {
  if ds.odbcConn != nil && ds.isTr {
    ds.Query("COMMIT");
  }
  ds.isTr = false
}

func (ds *ODBCDataSource) Close(forced bool) {
  if ds.isTr {
    ds.Rollback()
    ds.isTr = false
  }
  if ds.odbcConn != nil {
    ds.odbcConn.Close()
    ds.odbcConn = nil
  }
}
