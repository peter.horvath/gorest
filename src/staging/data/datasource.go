package data

// it is a logical data connection, from the viewpoint of the process
type DataSource interface {
  Connect()
  Begin()
  Rollback()
  Commit()
  Query(query string, params ...string) *QueryResult
  Close(forced bool)
}
