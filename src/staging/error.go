package table

type Error struct {
  Data
  errorMsg string
}

func NewError(errorMsg string) *Error {
  r := new(Error)
  r.errorMsg = errorMsg
  return r
}

func (qr *Error) HasError() bool {
  return true
}

func (qr *Error) GetError() string {
  return qr.errorMsg
}

func CastErrorToError(src interface{}) interface{} {
  return src.(*Error).Clone()
}

func CastErrorToScalar(src interface{}) interface{} {
  return NewScalar(src.(*Error).errorMsg, "")
}

func CastErrorToColumn(src interface{}) interface{} {
  return CastScalarToColumn(CastErrorToScalar(src))
}

func CastErrorToRow(src interface{}) interface{} {
  return CastScalarToRow(CastErrorToScalar(src))
}

func CastErrorToTable(src interface{}) interface{} {
  return CastRowToTable(CastErrorToRow(src))
}

func (qr *Error) Clone() *Data {
  return NewError(qr.errorMsg)
}
