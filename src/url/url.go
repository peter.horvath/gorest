package url

import (
  . "generic"
)

type Url struct {
  address string
  resource interface{}
}

type UrlSchemeHandler interface {
  Acquire()
  Release()
}

urlSchemeHandlers Map

func RegisterUrlSchemeHandler(scheme string, h UrlSchemeHandler) {
  panic("TBD")
}

func UnregisterUrlSchemeHandler(scheme string) {
  panic("TBD")
}
