/*
 * NEVER CALL YOUR PoolNode.* METHODS DIRECTLY!!! Use yourPool.Acquire(), yourPool.Release() and so on!
 *
 * TODO: old unused things removal!
 */

package pool

import (
  "time"
  "sync"
  "sync/atomic"

  "generic"
)

type PoolNode interface {
  OnAcquire()
  OnRelease()
  OnClose(forced bool)
}

type NewPoolNodeFunc func(pool *Pool) PoolNode

var DefaultTimeout int64 = 60 * 1000

type Pool struct {
  free, acquired *generic.Set
  createNode NewPoolNodeFunc
  lock sync.Mutex
  enabled bool
  timeout time.Duration
  cleanupTimer *time.Timer
}

var lastId uint64 = 0

type poolNode struct {
  poolNodeIf PoolNode
  pool *Pool
  lastPoolEventStamp time.Time
  id uint64
}

func NewPool(newPoolNodeFunc NewPoolNodeFunc) *Pool {
  r := new(Pool)
  r.createNode = newPoolNodeFunc
  r.free = generic.NewSet().SetComparator(comparePoolNode)
  r.acquired = generic.NewSet().SetComparator(comparePoolNode)
  r.SetTimeout(DefaultTimeout)
  r.enabled = true
  return r
}

func (p *Pool) GetTimeout() int64 {
  return int64(p.timeout / time.Millisecond)
}

func (p *Pool) SetTimeout(timeout int64) {
  p.timeout = time.Duration(timeout) * time.Millisecond
}

func (p *Pool) Acquire() *poolNode {
  if ! p.enabled {
    panic("")
  }
  p.lock.Lock()
  var pn *poolNode
  if p.free.IsEmpty() {
    p.lock.Unlock()
    pn = newpoolNode(p)
    p.lock.Lock()
  } else {
    pn = p.free.PopLast().(*poolNode)
    pn.lastPoolEventStamp = time.Now()
  }
  p.acquired.Add(pn)
  p.lock.Unlock()
  pn.poolNodeIf.OnAcquire()
  return pn
}

func (p *Pool) Release(pn *poolNode) {
  pn.poolNodeIf.OnRelease()
  p.lock.Lock()
  p.acquired.Remove(pn)
  pn.lastPoolEventStamp = time.Now()
  p.free.Add(pn)
  p.lock.Unlock()
  p.cleanup()
}

// only inside pool lock!
func (p *Pool) closeNode(pn *poolNode, forced bool) {
  p.lock.Unlock()
  pn.poolNodeIf.OnClose(forced)
  p.lock.Lock()
  if p.acquired.Has(pn) {
    p.acquired.Remove(pn)
  }
  if p.free.Has(pn) {
    p.free.Remove(pn)
  }
}

func (p *Pool) cleanup() {
  for {
    p.lock.Lock()
    if p.free.IsEmpty() {
      break
    }
    now := time.Now()
    pn := p.free.GetFirst().GetValue().(*poolNode)
    if pn.lastPoolEventStamp.Add(time.Duration(p.timeout) * time.Millisecond).After(now) {
      p.closeNode(pn, false)
    }
  }
  if p.free.IsEmpty() {
    p.cleanupTimer = nil
  } else {
    p.cleanupTimer = time.AfterFunc(p.timeout * time.Millisecond, func() {
      p.cleanup()
    })
  }
  p.lock.Unlock()
}

func (p *Pool) Destroy() {
  p.enabled = false
  p.lock.Lock()
  if p.cleanupTimer != nil {
    p.cleanupTimer.Stop()
    p.cleanupTimer = nil
  }
  p.lock.Unlock()
  for {
    p.lock.Lock()
    if p.acquired.IsEmpty() && p.free.IsEmpty() {
      p.lock.Unlock()
      break
    }
    if !p.free.IsEmpty() {
      p.closeNode(p.free.GetFirst().GetValue().(*poolNode), true)
      p.lock.Unlock()
      continue
    }
    if !p.acquired.IsEmpty() {
      p.closeNode(p.acquired.GetFirst().GetValue().(*poolNode), true)
      p.lock.Unlock()
      continue
    }
    p.lock.Unlock()
  }
}

// only inside pool lock!
func newpoolNode(p *Pool) *poolNode {
  pn := new(poolNode)
  pn.pool = p
  pn.lastPoolEventStamp = time.Now()
  pn.id = atomic.AddUint64(&lastId, uint64(1))
  pn.poolNodeIf = p.createNode(p)
  return pn
}

/*
func (t *poolNode) Compare(n primitive.Comparable) int {
  return comparePoolNode(t, n.(*poolNode))
}*/

func comparePoolNode(A interface{}, B interface{}) int {
  a := A.(*poolNode)
  b := B.(*poolNode)
  if a == b {
    return 0
  } else if a.lastPoolEventStamp.Before(b.lastPoolEventStamp) {
    return -1
  } else if a.lastPoolEventStamp.After(b.lastPoolEventStamp) {
    return 1
  } else if a.id < b.id {
    return -1
  } else if a.id > b.id {
    return 1
  } else {
    panic("") // id is auto incrementing, so this is unreachable
  }
}
