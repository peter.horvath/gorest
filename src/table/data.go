package table

import (
  "generic"
)

type Data struct {
  generic.Object
  err generic.LinkedList
}

func (d *Data) HasError() bool {
  return !d.err.IsEmpty()
}

func (d *Data) GetError() string {
  if d.err.IsEmpty() {
    return ""
  } else {
    return d.err.Join("\n")
  }
}

func (d *Data) GetErrors() *generic.LinkedList {
  return &d.err
}
