package table

import (
  "reflect"

  "generic"
)

type Table struct {
  Data
  Columns generic.MultiTuple
  Rows generic.TreeList
}

var TableType *generic.Type

func NewTable() *Table {
  return new(Table).Init()
}

func (t *Table) Init() *Table {
  t.SetType(TableType)
  t.Columns.Init()
  t.Rows.Init()
  return t
}

func (t *Table) AddColumn(name string, c *Column) *Table {
  t.Columns.Add(name, c)
  return t
}

func (t *Table) AddRow(r *Row) *Table {
  t.Rows.Add(r)
  return t
}

func Convert_Table_Row(t interface{}) interface{} {
  return t.(*Table).Rows.At(0).GetValue().(*Row)
}

func Convert_Table_Column(t interface{}) interface{} {
  return t.(*Table).Columns.At(0).GetValue().(*Column)
}

func initTable() {
  TableType = generic.RegisterType(reflect.TypeOf((*Table)(nil)).Elem())
}

func initTableCoca() {
  generic.RegisterConverter(TableType, RowType, Convert_Table_Row)
  generic.RegisterConverter(TableType, ColumnType, Convert_Table_Column)
}
