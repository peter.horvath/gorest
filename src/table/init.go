package table

func Init() {
  initRow()
  initColumn()
  initTable()
  initRowCoca()
  initColumnCoca()
  initTableCoca()
}
