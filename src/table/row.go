package table

import (
  "reflect"

  "generic"
)

type Row struct {
  Data
  fields generic.MultiTuple
}

var RowType *generic.Type

func NewRow() *Row {
  return new(Row).Init()
}

func (r *Row) Init() *Row {
  r.SetType(RowType)
  return r
}

func (r *Row) GetFields() *generic.MultiTuple {
  return &r.fields
}

func Convert_Row_Table(r interface{}) interface{} {
  t := NewTable()
  t.AddRow(r.(*Row))
  return t
}

func Convert_Row_Column(r interface{}) interface{} {
  return Convert_Table_Row(Convert_Row_Table(r))
}

func initRow() {
  RowType = generic.RegisterType(reflect.TypeOf((*Row)(nil)).Elem())
}

func initRowCoca() {
  generic.RegisterConverter(RowType, TableType, Convert_Row_Table)
  generic.RegisterConverter(RowType, ColumnType, Convert_Row_Column)
}
