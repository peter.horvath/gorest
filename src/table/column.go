package table

import (
  "reflect"

  "generic"
)

type Column struct {
  Data
  dataType *generic.Type
  data generic.TreeList
}

var ColumnType *generic.Type

func NewColumn(type_ *generic.Type) *Column {
  return new(Column).Init(type_)
}

func (c *Column) Init(type_ *generic.Type) *Column {
  c.SetType(ColumnType)
  return c
}

func (c *Column) GetType() *generic.Type {
  return c.dataType
}

func (c *Column) GetData() *generic.TreeList {
  return &c.data
}

func Convert_Column_Row(c interface{}) interface{} {
  return Convert_Table_Row(Convert_Column_Table(c))
}

func Convert_Column_Table(c interface{}) interface{} {
  t := NewTable()
  t.AddColumn("", c.(*Column))
  return t
}

func initColumn() {
  ColumnType = generic.RegisterType(reflect.TypeOf((*Column)(nil)).Elem())
}

func initColumnCoca() {
  generic.RegisterConverter(ColumnType, RowType, Convert_Column_Row)
  generic.RegisterConverter(ColumnType, TableType, Convert_Column_Table)
}
