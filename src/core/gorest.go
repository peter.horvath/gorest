package gorest

import (
  . "generic"
)

type Gorest struct {
  handlersByType *generic.Map
  handlersByName *generic.Map
}

func NewGorest() *Gorest {
  return new(Gorest).Init()
}

func (g *Gorest) Init() *Gorest {
  g.handlers = NewMap()
}

func (g *Gorest) RegisterHandler(handler *Handler) {
  if g.handlers.Has(handler.GetName()) {
    panic("scheme " + scheme + " already registered")
  }
  g.handlers.Set(handler.GetName(), handler)
}

func (g *Gorest) UnregisterHandler(handler *Handler) {
  if !g.handlers.Has(handler.GetName()) {
    panic("scheme " + scheme + " is not registered, can not unregister")
  }
  g.handlers.Unset(handler.GetName())
}

func (g *Gorest) Process(o *Object) {
  
}

/*
 Input: http://something/entity/id
 -> calls HttpRequest Handler, outputs: GorestRequest
 Results: "GorestRequest", entity, id
 -> calls GorestResponse Handler, outputs: DataRow
 Results: "DataRow", dataRow
 -> calls DataRowToJson Handler, outputs: Json

Task: input, output
*/
