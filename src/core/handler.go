package gorest

import (
  . "generic"
)

type Handler struct {
  Object
  name string
  requestType Type
  process func(request *Object) *Object
}
