package crud

import (
  "generic"
)

type Crud interface {
  Create() int
  Read(entity generic.String, id generic.Int64, field generic.String, params generic.Map) *generic.Map
  Update(entity generic.String, id generic.Int64, params generic.Map)
  Delete(entity generic.String, id generic.Int64)
}

type SQLCrud struct {
  Crud
}

func (*SQLCrud) Create() int {
  //TODO
  return 0
}

func (*SQLCrud) Read(entity generic.String, id generic.Int64, field generic.String, params generic.Map) *generic.Map {
  //TODO
  return generic.NewMap()
}

func (*SQLCrud) Update(entity generic.String, id generic.Int64, params generic.Map) {
  // TODO
}

func (*SQLCrud) Delete(entity generic.String, id generic.Int64) {
  // TODO
}
