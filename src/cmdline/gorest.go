package main

import (
  "flag"
  "fmt"

  "odbc"

  . "generic"
)

func isSqlOk(err *odbc.ODBCError) {
  if err != nil {
    panic ("err, sqlstate: " + err.SQLState + " NativeError: " + As(err.NativeError, Type_string).(string) + " ErrorMessage: " + err.ErrorMessage)
  }
}

func execSql(sql string) {
  conn, err := odbc.Connect("DSN=gorest")
  isSqlOk(err)
  stmt, err := conn.Prepare(sql)
  isSqlOk(err)
  err = stmt.Execute()
  isSqlOk(err)
  rows, _ := stmt.FetchAll()
  for i, row := range rows {
    println(i, row.GetInt(0))
  }
  stmt.Close()
  conn.Close()
}

func execPoolSql(poolSql string) {
  panic("TBD")
}

// pool://odbc://
// pool://
// odbc://
// pool://odbc://DSN=gorest
// xml://
func main() {
  sql := flag.String("sql", "", "sql statement to execute")
  poolSql := flag.String("poolsql", "", "sql statement to execute through pool")

  flag.Parse()

  fmt.Printf("sql: %s poolSql: %s\n", *sql, *poolSql)

  if *sql != "" {
    execSql(*sql)
  } else if *poolSql != "" {
    execPoolSql(*poolSql)
  } else {
    execSql("SELECT * FROM test")
  }
}
