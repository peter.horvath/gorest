GOPATH:=$(CURDIR)
GO:=go-6

default: odbc

.PHONY: odbc
odbc:
	$(GO) build
